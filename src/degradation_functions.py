# -*- coding: utf-8 -*-
"""
Created on Wed Jul 11 10:05:59 2018

@author: sjangra
"""

import numpy as np

def stress_cur(Ich,Ich_ref,beta,C0):
    return np.exp(beta*(Ich - Ich_ref)/C0)

def stress_temp(Ea,T):
    Rg = 8.314      # Gas constant
    T_ref = 298.15      # Reference temperature
    return np.exp(-Ea*(1/T - 1/T_ref)/Rg)

def stress_SOC(SOC,alpha,k0):
    # Defining parameters
  F = 96485.3329  # Faraday Const. (C/mol)
  T_ref = 298.15 # Reference temperature (Kelvins)
  Rg = 8.314 # Gas constant (J/(mol-K))
  Ua_ref = 0.12330 # Reference Anode potential (Volts)
  xa_0 = 8.5e-3  # Stoichiometry at 0% SOC
  xa_100 = 7.8e-1  # Stoichiometry at 1000% SOC
  
  # Defining stoichiometry as a function of SOC
  xa = xa_0 + SOC*(xa_100 - xa_0)
  
  # Calcualting Anode potential from input SOC
  Ua = 0.6379 + 0.5416*np.exp(-305.5309*xa) + 0.044*np.tanh(-(xa - 0.1958)/0.1088) \
  - 0.1978*np.tanh((xa - 1.0571)/0.0854) - 0.6875*np.tanh((xa + 0.0117)/0.0529) \
  - 0.0175*np.tanh((xa - 0.5692)/0.0875)
 
  # Calcualting stress factor
  return np.exp(alpha*F*(Ua_ref - Ua)/T_ref/Rg) + k0
   