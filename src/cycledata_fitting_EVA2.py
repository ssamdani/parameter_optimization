# -*- coding: utf-8 -*-
"""
Created on Fri Jul 20 13:56:24 2018

@author: sjangra
"""

# TODO import from jupyter notebook
import numpy as np
import scipy.io       # To import timeseries_data from .mat file
import scipy.optimize as opt
import matplotlib.pyplot as plt
import pandas as pd
import timeit

# import sys,os
# sys.path.append('..')
import src.degradation_functions as dfunc

def cap_loss(data_caploss, data_expdata, params, x):
    I = np.array(data_expdata["I"])
    temp = np.array(data_expdata["Temp"])
    SOC = np.array(data_expdata["SOC"])
    charge_thr = np.array(data_expdata["Charge_thr"])
    tot_thr = np.array(data_expdata["Tot_thr"])
    t = np.array(data_expdata["Time [Hr]"])
    cycle_index = np.array(data_expdata["cycle_index"])
    cap_loss_exp = np.array(data_caploss["Cap Loss"])
    cap_loss_model = np.array(data_caploss["Qtot per cycle"])

    # Defining parameters
    C0 = 77
    Ich_ref = 77

    k_cal_ref = float(params["k_cal_ref"])
    Ea_cal = float(params["Ea_cal"])
    alpha = float(params["alpha"])
    k0 = float(params["k0"])

    k_cyc_highT_ref = x[0]
    Ea_cyc_highT = x[1]

    k_cyc_lowT_ref = 3.01e-4
    beta_lowT = 2.64
    Ea_cyc_lowT = -5.55e4

    k_cyc_lowT_highSOC_ref = 2.03e-6
    beta_lowT_highSOC = 7.84
    Ea_cyc_lowT_highSOC = -2.33e5
    SOC_ref = 0.82

    # Initialization
    K_cal = np.zeros_like(t)
    K_cyc_highT = np.zeros_like(t)
    K_cyc_lowT = np.zeros_like(t)
    K_cyc_lowT_highSOC = np.zeros_like(t)
    s_cyc_lowT_temp = np.zeros_like(t)
    s_cyc_lowT_highSOC_temp = np.zeros_like(t)
    Q_cal = np.zeros_like(t)
    Q_cyc = np.zeros_like(t)
    Q_tot = np.zeros_like(t)
    Q_cyc_highT = np.zeros_like(t)
    Q_cyc_lowT = np.zeros_like(t)
    Q_cyc_lowT_highSOC = np.zeros_like(t)
    s_cyc_lowT_cur = np.zeros_like(t)
    s_cyc_lowT_highSOC_cur = np.zeros_like(t)
    s_cal_soc = np.zeros_like(t)

    # Calendar loss
    s_cal_soc = dfunc.stress_SOC(SOC, alpha, k0)
    K_cal = (
        k_cal_ref * dfunc.stress_temp(Ea_cal, temp) * s_cal_soc
    )  # Stress factor (Temp,SOC)
    Q_cal = np.zeros_like(t)
    Q_cal[1:] = np.cumsum(K_cal[1:] * (np.ediff1d(np.sqrt(t))))

    # Cycling loss
    s_cyc_lowT_temp = dfunc.stress_temp(Ea_cyc_lowT, temp)
    s_cyc_lowT_highSOC_temp = dfunc.stress_temp(Ea_cyc_lowT_highSOC, temp)

    s_cyc_lowT_cur = dfunc.stress_cur(I, Ich_ref, beta_lowT, C0)
    s_cyc_lowT_cur[I < 0] = 0
    s_cyc_lowT_highSOC_cur = dfunc.stress_cur(I, Ich_ref, beta_lowT_highSOC, C0)
    s_cyc_lowT_highSOC_cur[I < 0] = 0

    K_cyc_highT = k_cyc_highT_ref * dfunc.stress_temp(Ea_cyc_highT, temp)
    K_cyc_lowT = k_cyc_lowT_ref * s_cyc_lowT_cur * s_cyc_lowT_temp
    K_cyc_lowT_highSOC = (
        k_cyc_lowT_highSOC_ref
        * s_cyc_lowT_highSOC_cur
        * s_cyc_lowT_highSOC_temp
        * ((np.sign(SOC - SOC_ref) + 1) / 2)
    )

    Q_cyc_highT = np.zeros_like(t)
    Q_cyc_highT[1:] = np.cumsum(K_cyc_highT[1:] * np.ediff1d(np.sqrt(tot_thr)))

    Q_cyc_lowT = np.zeros_like(t)
    Q_cyc_lowT[1:] = np.cumsum(K_cyc_lowT[1:]* np.ediff1d(np.sqrt(charge_thr)))

    Q_cyc_lowT_highSOC = np.zeros_like(t)
    Q_cyc_lowT_highSOC[1:] = np.cumsum(K_cyc_lowT_highSOC[1:] * np.ediff1d(charge_thr))

    # Total capacity loss
    Q_cyc = Q_cyc_highT + Q_cyc_lowT + Q_cyc_lowT_highSOC  # Total Cycling losses
    Q_tot = 100 * (Q_cal + Q_cyc)  # Total Capacity loss

    # find index locations where cycle changes as this is the only experimental timeseries_data we have
    cyc_change_index = np.ediff1d(cycle_index,to_end=0)>0
    # get loss values for these index changes
    Q_tot_percycle  = Q_tot[cyc_change_index]
    Q_tot_percycle[0] = 0

    return np.sqrt(np.mean(np.square(Q_tot_percycle - cap_loss_exp)))


def read_data(flocation, fnames):
    parameters = pd.read_csv(
        "../timeseries_data/cal_params_fitted.csv"
    )
    df_list = []
    for fn in fnames:
        data_caploss = pd.read_csv(flocation + fn + "_caploss.csv")
        data_expdata = pd.read_csv(flocation + fn + "_expdata.csv")
        df_list.append(dict(caploss=data_caploss, expdata=data_expdata))
    inp_dict = dict(params=parameters, data=df_list)
    return inp_dict

#%% Error function
def error(x):
    #     params = pd.read_csv('../timeseries_data/cal_params_fitted.csv')
    params = inputdict['params']
    err = np.zeros(n_test_conditions)
    for i in range(n_test_conditions):
        datadict = inputdict['timeseries_data'][i]
        data_caploss = datadict['caploss']
        data_expdata = datadict['expdata']
        err[i] = cap_loss(data_caploss,data_expdata,params,x)

    error_tot = np.sum(err)
    return error_tot


if __name__ == '__main__':
    file_dirc = '../timeseries_data/eva2_20180717/Data fitting/'

    file_names = ['HA212_00007__7-10-1-P73B__25oC__Cby3__2.75-4.20__20180615170813',\
                      'HA212_00009__7-10-2-P73B__25oC__Cby3__2.75-4.20__20180615170822',\
                      'HA212_00512__7-9-2-P73B__25oC__1C__2.75-4.20__20180615170748',\
                      'HA212_03677__7-7-1-P73B__25oC__Cby3__2.75-4.20__20180615170631',\
                      'HA212_03734__7-7-2-P73B__25oC__Cby3__2.75-4.20__20180615170641',\
                      'HA212_00058__1-11-3-P73B__45oC__Cby3__2.75-4.20__20180615171240',\
                      'HA212_00109__1-11-4-P73B__45oC__Cby3__2.75-4.20__20180615171320']
    n_test_conditions = len(file_names)
    inputdict = read_data(file_dirc, file_names)
    xsid = np.array([9.823103175997052348e-05,3.376641222892397491e+04])
    er = error(xsid)
    print(er)
