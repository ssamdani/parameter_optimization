# -*- coding: utf-8 -*-
"""
Created on Tue Jul 24 13:18:36 2018

@author: sjangra
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

#%% Importing experimental timeseries_data

file_dirc = 'C:/Users/sjangra.FARASIS/OneDrive - Farasis Energy Inc/Documents/service_life_model_py/Experimental Data/EVA2 calendar/'
file_name = ['60%SOC@45C','80%SOC@45C','100%SOC@45C',\
             '40%SOC@52,5C','60%SOC@52,5C','80%SOC@52,5C',\
             '60%SOC@60C','100%SOC@60C']

exp_data_dict= [dict() for x in range(len(file_name))]
for i in range(len(file_name)):
    exp_data = pd.read_csv(file_dirc + file_name[i] +'.csv')
    time_exp = exp_data['Storageduration/d']
    cap_loss_exp = exp_data['Capacity Loss']
    time = np.linspace(0,int(time_exp[len(time_exp)-1])*24,int(time_exp[len(time_exp)-1])*24+1)
    cap_loss = np.interp(time/24,time_exp,cap_loss_exp)
    exp_data_dict[i] = {'time':time,'cap_loss':cap_loss}
    np.savetxt(file_dirc+file_name[i]+'_interp.csv',np.array((time,cap_loss)).T,header = "Time[Hr],Cap_loss",delimiter = ',',comments = '')
    

#%% Figures

exp_data_interp_dict= [dict() for x in range(len(file_name))]
for i in range(len(file_name)):
    exp_data = pd.read_csv(file_dirc + file_name[i] +'.csv')
    exp_data_dict[i] = {'time':exp_data['Storageduration/d'],'cap_loss':exp_data['Capacity Loss']}
    exp_interp_data = pd.read_csv(file_dirc + file_name[i] +'_interp.csv')
    exp_data_interp_dict[i] = {'time':exp_interp_data['Time[Hr]'],'cap_loss':exp_interp_data['Cap_loss']}
    plt.figure(i)
    plt.plot(exp_data_dict[i]['time'],exp_data_dict[i]['cap_loss'])
    plt.plot(exp_data_interp_dict[i]['time']/24,exp_data_interp_dict[i]['cap_loss'],linestyle = '--')
    plt.legend(('Exp','Interp'))