import pandas as pd
import numpy as np
import csvtool


def filter_end_of_cycle_outlier(cell_df):
    """lot of discharge capacity data has outliers at end of the cycle due to incomplete test
    this is an effort to filter it out by setting all values below 70% threshold to previous value
    """
    discap_df = cell_df.copy()
    dmax = discap_df.max()
    outlier_locations = discap_df < 0.7 * dmax
    shift_discap = discap_df.shift(1)
    discap_df[outlier_locations] = shift_discap[outlier_locations]
    return discap_df.values


def create_csv_files(file_name, summary_data, csv_dirc):
    summary_data = summary_data[(10 - 1) :]
    dis_cap_exp = filter_end_of_cycle_outlier(summary_data["Discharge Capacity [Ah]"])
    max_dis_capacity = dis_cap_exp.max()
    cap_loss_exp = (max_dis_capacity - dis_cap_exp) / max_dis_capacity * 100
    cap_loss_exp = np.insert(cap_loss_exp, 0, 0)
    csv_caploss_path = csv_dirc + file_name + "_caploss.csv"
    csvtool.dicttocsv(["Cap Loss"], {"Cap Loss": cap_loss_exp}, csv_caploss_path)


if __name__ == "__main__":
    fnames = ["172"]
    csv_dirc = (
        r"C:\Users\ssamdani\OneDrive - Farasis Energy Inc\Documents\repos\parameter_optimization\data\b0_sample\processed"
        + "/"
    )
    dirc = r"C:\Users\ssamdani\OneDrive - Farasis Energy Inc\Documents\repos\parameter_optimization\notebooks\data_to_process"
    for f in fnames:
        print(f"workign on file:{f}")
        summary = pd.read_csv(f"{dirc}/{f}__extradata.csv")
        create_csv_files(f, summary, csv_dirc)
