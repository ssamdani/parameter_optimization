"""
"""

import numpy as np
import pandas as pd
import timeit
import json
from itertools import repeat
import argparse
import scipy.optimize as opt
import matplotlib.pyplot as plt
from multiprocessing import Pool
import sys

sys.path.append(r"../..")
from degradation_model.src.simulation_model import DegradationModelECS2018


# global order
# global n_test_conditions


def cap_loss(data_caploss, data_expdata, params, x, order, plot=False):
    time_col, soc_col, temp_col, current_col = (
        "Time [hr]",
        "SOC []",
        "temp",
        "Current [A]",
    )
    cap_loss_exp = data_caploss["Cap Loss"].values
    modeldf = pd.DataFrame()
    modeldf[time_col] = data_expdata["Time [Hr]"]
    modeldf[soc_col] = data_expdata["SOC"]
    modeldf[temp_col] = data_expdata["Temp"] - 273.15
    modeldf[current_col] = data_expdata["I"]
    for i, k in enumerate(order):
        params[k] = x[i]

    model = DegradationModelECS2018(TestInputs(params, modeldf), "temp", None)
    model.run()
    cycle_index = data_expdata.cycle_index
    change_cycle = np.ediff1d(cycle_index.values, to_end=0.0) > 0.99
    model_loss = 100 * model.Q_tot[change_cycle]
    highSOC_loss = 100 * model.Q_cyc_lowT_highSOC[change_cycle]
    len_exp_data = len(cap_loss_exp)
    if sum(change_cycle) > len_exp_data:
        model_loss = model_loss[-1 * len_exp_data:]
    if plot:
        fig, axes = plt.subplots()
        axes.plot(model_loss, ',', label="model")
        axes.plot(cap_loss_exp, '.', label="exp")
        axes.plot(highSOC_loss, '.', label="highSOC")
        plt.show(fig)
        print(f"model: {model_loss[-1]}, exp: {cap_loss_exp[-1]}")
    rmserror = np.sqrt(np.mean(np.square(model_loss - cap_loss_exp)))
    return rmserror


def read_data(flocation, fnames):
    with open("../data/b0_sample/processed/EVA2_10deg_fitting.json") as f:
        # if high SOC needs to be left out then use EVA2_10deg_fitting_no_high_SOC
        parameters = json.load(f)
    df_list = []
    for fn in fnames:
        data_caploss = pd.read_csv(flocation + fn + "_caploss.csv")
        data_expdata = pd.read_csv(flocation + fn + "_expdata.csv")
        df_list.append(dict(caploss=data_caploss, expdata=data_expdata))
    inp_dict = dict(params=parameters, data=df_list)
    return inp_dict


class TestInputs:
    def __init__(self, cell_parameters, simulation_period_df):
        self.cell_parameters = cell_parameters
        self.simulation_period_df = simulation_period_df
        self.input_config = dict(lowT_high_SOC_parameter_modifier=1)


def error(x):
    arguments = zip(range(n_test_conditions), repeat(order), repeat(x), repeat(inputdict))
    pool = Pool()
    err = pool.starmap(run_cap_loss, arguments)
    print(f'{sum(err)},{tuple(x)}')
    return sum(err)


def run_cap_loss(icase, ord, xvalues, idict):
    params = idict["params"]
    datadict = idict["data"][icase]
    data_caploss = datadict["caploss"]
    data_expdata = datadict["expdata"]
    return cap_loss(data_caploss, data_expdata, params, xvalues, ord, False)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "file_name_json", help="Provide a json file with list of file names."
    )
    args = parser.parse_args()
    print(args.file_name_json)
    with open(args.file_name_json) as f:
        flist = json.load(f)

    file_names = flist["file_names"]
    file_dirc = "../data/b0_sample/processed/"
    print(file_names)
    n_test_conditions = len(file_names)
    order = """k_cyc_lowT_ref
        Ea_cyc_lowT
        beta_lowT
        k_cyc_lowT_highSOC_ref
        Ea_cyc_lowT_highSOC
        beta_lowT_highSOC
        """.split()
    start = timeit.default_timer()
    inputdict = read_data(file_dirc, file_names)
    # x0 = []
    # for i, k in enumerate(order):
    #     x0.append(inputdict["params"][k])
    bnds = (
        [0, 1e-3],
        [-60000, 0],
        [3.92, 20],
        [0, 1e-5],
        [-60000, 0],
        [0, 20],
    )

    # evaluate = True
    evaluate = False
    if evaluate:
        make_plots = True
        error(x0)
    else:
        make_plots = False
        # results = opt.minimize(error, x0,
        #                        bounds=bnds
        #                        )
        results = opt.differential_evolution(error,
                                             bounds=bnds,
                                             )
        xf_cycle = results.x
        stop = timeit.default_timer()
        print(f"run time:{stop - start}")
        print(xf_cycle)
        optimized_params = pd.DataFrame(data=xf_cycle, index=order)
        optimized_params.to_csv("results.csv")
        optimized_params.to_json("results.json")
        for i, p in enumerate(order):
            print(f'"{order[i]}":{xf_cycle[i]},')
