# -*- coding: utf-8 -*-
"""
Created on Fri Jul 20 13:56:24 2018

@author: sjangra
"""

mport numpy as np
import scipy.io       # To import timeseries_data from .mat file
import scipy.optimize as opt
import matplotlib.pyplot as plt
import degradation_functions as df
import pandas as pd
import timeit

start = timeit.default_timer()
#%% Defining capacity loss function

def cap_loss(data_caploss,data_expdata,params,x):
    I = np.array(data_expdata['I'])
    temp = np.array(data_expdata['Temp'])
    SOC = np.array(data_expdata['SOC'])
    charge_thr = np.array(data_expdata['Charge_thr'])
    tot_thr = np.array(data_expdata['Tot_thr'])
    t = np.array(data_expdata['Time [Hr]'])
    cycle_index = np.array(data_expdata['cycle_index'])
    cap_loss_exp = np.array(data_caploss['Cap Loss'])
    cap_loss_model = np.array(data_caploss['Qtot per cycle'])
    
    # Defining parameters
    k_cal_ref = float(params['k_cal_ref'])
    k_cyc_highT_ref = x[0]
    k_cyc_lowT_ref = 3.01e-4
    k_cyc_lowT_highSOC_ref = 2.03e-6
    Ea_cal = float(params['Ea_cal'])
    Ea_cyc_highT = x[1]
    Ea_cyc_lowT = -5.55e4
    Ea_cyc_lowT_highSOC = -2.33e5
    beta_lowT = 2.64
    beta_lowT_highSOC = 7.84
    SOC_ref = 0.82
    C0 = 77
    alpha = float(params['alpha'])
    k0 = float(params['k0'])
    Ich_ref = 77
    
    # Initialization
    K_cal = np.zeros_like(t)
    K_cyc_highT = np.zeros_like(t)
    K_cyc_lowT = np.zeros_like(t)
    K_cyc_lowT_highSOC = np.zeros_like(t)
    s_cyc_lowT_temp = np.zeros_like(t)
    s_cyc_lowT_highSOC_temp = np.zeros_like(t)
    Q_cal = np.zeros_like(t)
    Q_cyc = np.zeros_like(t)
    Q_tot = np.zeros_like(t)
    Q_cyc_highT = np.zeros_like(t)
    Q_cyc_lowT =np.zeros_like(t)
    Q_cyc_lowT_highSOC =np.zeros_like(t)
    s_cyc_lowT_cur =np.zeros_like(t)
    s_cyc_lowT_highSOC_cur =np.zeros_like(t)
    s_cal_soc =np.zeros_like(t)
    Q_tot_percycle = []
    
    for j in range(1,len(t)):
                
        # Calendar loss
        s_cal_soc[j] = df.stress_SOC(SOC[j],alpha,k0)
        K_cal[j] = k_cal_ref*df.stress_temp(Ea_cal,temp[j])*s_cal_soc[j]  # Stress factor (Temp,SOC)
        Q_cal[j] = K_cal[j]*(np.sqrt(t[j]) - np.sqrt(t[j-1])) + Q_cal[j-1]
        
        # Cycling loss
        s_cyc_lowT_temp[j] = df.stress_temp(Ea_cyc_lowT,temp[j])
        s_cyc_lowT_highSOC_temp[j] = df.stress_temp(Ea_cyc_lowT_highSOC,temp[j])

        if I[j]>0: # Charge current
            s_cyc_lowT_cur[j] = df.stress_cur(I[j],Ich_ref,beta_lowT,C0)
            s_cyc_lowT_highSOC_cur[j] = df.stress_cur(I[j],Ich_ref,beta_lowT_highSOC,C0)
        else:  # Discharge current or no current
            s_cyc_lowT_cur[j] = 0
            s_cyc_lowT_highSOC_cur[j] = 0
    
    
        if temp[j]>298.15: # High temperature
            K_cyc_highT[j] = k_cyc_highT_ref*df.stress_temp(Ea_cyc_highT,temp[j])
            K_cyc_lowT[j] = 0
            K_cyc_lowT_highSOC[j] = 0
        elif temp[j]<298.15: # Low Temperature
            K_cyc_highT[j] = 0
            K_cyc_lowT[j] = k_cyc_lowT_ref*s_cyc_lowT_cur[j]*s_cyc_lowT_temp[j]
            K_cyc_lowT_highSOC[j] = k_cyc_lowT_highSOC_ref*s_cyc_lowT_highSOC_cur[j] \
            *s_cyc_lowT_highSOC_temp[j]*((np.sign(SOC[j]-SOC_ref)+1)/2)
        else:  # Temp = 25deg Celcius
            K_cyc_highT[j] = k_cyc_highT_ref*df.stress_temp(Ea_cyc_highT,temp[j])
            K_cyc_lowT[j] = k_cyc_lowT_ref*s_cyc_lowT_cur[j]*s_cyc_lowT_temp[j]
            K_cyc_lowT_highSOC[j] = k_cyc_lowT_highSOC_ref*s_cyc_lowT_highSOC_cur[j] \
            *s_cyc_lowT_highSOC_temp[j]*((np.sign(SOC[j]-SOC_ref)+1)/2)
    
        Q_cyc_highT[j] = K_cyc_highT[j]*(np.sqrt(tot_thr[j])-np.sqrt(tot_thr[j-1]))+ Q_cyc_highT[j-1]
        Q_cyc_lowT[j] = K_cyc_lowT[j]*(np.sqrt(charge_thr[j])-np.sqrt(charge_thr[j-1]))+ Q_cyc_lowT[j-1]
        Q_cyc_lowT_highSOC[j] = K_cyc_lowT_highSOC[j]*(charge_thr[j]-charge_thr[j-1])+ Q_cyc_lowT_highSOC[j-1]
        
        # Total capacity loss
        Q_cyc[j] = Q_cyc_highT[j] + Q_cyc_lowT[j] + Q_cyc_lowT_highSOC[j]  # Total Cycling losses
        Q_tot[j] = Q_cal[j] + Q_cyc[j]  # Total Capacity loss
        
        if cycle_index[j]!= cycle_index[j-1]:
            Q_tot_percycle.append(Q_tot[j-1])
    
    del[Q_tot_percycle[0]]     
    Q_tot_percycle.insert(0,0)
    Q_tot_percycle = [x*100 for x in Q_tot_percycle]
    
    # Error calculation
    error_cap_exp = Q_tot_percycle - cap_loss_exp 
    error_cap_model = Q_tot_percycle - cap_loss_model 
    
    return {'error_cap_exp':error_cap_exp, 'error_cap_model':error_cap_model,'Q_tot_percycle':Q_tot_percycle}

#%% Error function
def error(x):  
#    cal_params = scipy.io.loadmat('Matlab workspace/Saved workspace/daimler_sqrt_parameters')
#    xf = cal_params['xf']   # Calendar ageing parameters
    params = pd.read_csv('C:/Users/sjangra.FARASIS/OneDrive - Farasis Energy Inc/Documents/service_life_model_py/Experimental Data/EVA2 calendar/cal_params_fitted.csv')
    file_dirc = 'C:/Users/sjangra.FARASIS/OneDrive - Farasis Energy Inc/Documents/service_life_model_py/Experimental Data/eva2_20180717/Data fitting/'
    
    file_name = ['HA212_00007__7-10-1-P73B__25oC__Cby3__2.75-4.20__20180615170813',\
                  'HA212_00009__7-10-2-P73B__25oC__Cby3__2.75-4.20__20180615170822',\
                  'HA212_00512__7-9-2-P73B__25oC__1C__2.75-4.20__20180615170748',\
                  'HA212_03677__7-7-1-P73B__25oC__Cby3__2.75-4.20__20180615170631',\
                  'HA212_03734__7-7-2-P73B__25oC__Cby3__2.75-4.20__20180615170641',\
                  'HA212_00058__1-11-3-P73B__45oC__Cby3__2.75-4.20__20180615171240',\
                  'HA212_00109__1-11-4-P73B__45oC__Cby3__2.75-4.20__20180615171320']
    
#    x = pd.read_csv(file_dirc + 'highT_parameters_newcal.csv')
#    x = np.array([x['K_cyc_ref_highT'],x['Ea_highT']])
    cap_loss_return = [dict() for x in range(len(file_name))]
    error = np.zeros(len(file_name))
    
    for i in range(0,len(file_name)):
        data_caploss = pd.read_csv(file_dirc + file_name[i] + '_caploss.csv')
        data_expdata = pd.read_csv(file_dirc  + file_name[i] + '_expdata.csv')   
        cap_loss_return[i] = cap_loss(data_caploss,data_expdata,params,x)
        error[i] = np.sqrt(np.mean(np.square(cap_loss_return[i]['error_cap_exp'])))
        
    error_tot = np.sum(error)
    return error_tot

#%% Fitting function
x0 = np.array([1.46e-4,3.27e4])
xf_cycle = opt.fmin(error,x0)


#,maxiter=10000,maxfun = 10000
#for i in range(0,len(file_name)):
#    data_caploss = pd.read_csv(file_dirc + file_name[i] + '_caploss.csv')
#    plt.figure(i)
#    plt.plot(cap_loss_return[i]['Q_tot_percycle'])
#    plt.plot(data_caploss['Cap Loss'])

#xf_cycle = np.array([xf_cycle])
#np.savetxt(file_dirc+'highT_parameters_newcal.csv',xf_cycle,header = "K_cyc_ref_highT,Ea_highT",delimiter = ',',comments="")    

stop = timeit.default_timer()
print(stop - start)