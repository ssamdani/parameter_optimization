from multiprocessing import Pool
import numpy as np
import pandas as pd
import csvtool


def filter_end_of_cycle_outlier(cell_df):
    """lot of discharge capacity data has outliers at end of the cycle due to incomplete test
    this is an effort to filter it out by setting all values below 70% threshold to previous value
    """
    discap_df = cell_df.copy()
    dmax = discap_df.max()
    outlier_locations = discap_df < 0.7 * dmax
    shift_discap = discap_df.shift(1)
    discap_df[outlier_locations] = shift_discap[outlier_locations]
    return discap_df.values


def create_csv_files(file_name, timeseries_data, summary_data, csv_dirc):
    # Calculating starting index
    #cycle_index = timeseries_data["Cycle Index"].values
    #no editing of first 10 cycles necessary in some of the data sets
    #  start_ind = np.where(cycle_index == 10)
    # # np.where returns a tuple so it will be indexed at 0 twice
    #  start_ind = start_ind[0]
    #  print("Starting index is: ", start_ind[0] - 2)
    #  timeseries_data = timeseries_data[start_ind[0] - 2 :]
    #  summary_data = summary_data[(10 - 1) :]

    # allocating timeseries_data
    I = timeseries_data["Current [A]"]
    t = timeseries_data["Time [hr]"]
    volt = timeseries_data["Voltage [V]"]
    temp = 273.15 + np.mean(
        [timeseries_data["T3 [oC]"], timeseries_data["T4 [oC]"]], axis=0
    )
    cap_step = timeseries_data["Step Capacity [Ah]"].values
    step_index = timeseries_data["Step Index"].values
    cycle_index = timeseries_data["Cycle Index"].values
    dis_cap_exp = filter_end_of_cycle_outlier(summary_data["Discharge Capacity [Ah]"])

    # converting 1d array to 2d array for consistency
    t = t[np.newaxis, :]
    temp = temp[np.newaxis, :]
    I = I[np.newaxis, :]

    SOC = np.zeros(np.size(t, 1))
    charge_thr = np.zeros(np.size(t, 1))
    tot_thr = np.zeros(np.size(t, 1))
    charge = np.zeros(np.size(t, 1))
    cap = np.zeros(np.size(t, 1))
    SOC_step = np.zeros(np.size(t, 1))
    step_cap_diff = np.zeros(np.size(t, 1))
    cap_step_temp = np.zeros(np.size(t, 1))
    SOC[0] = 0.0
    SOC_step[0] = SOC[0]
    cap[:] = 77
    SOC_corr_count = 0
    index = 0
    corr_inds = []

    # %% Charge calculation through current
    # Makes step cap difference equal to zero when the current is zero. Also makes the step cap
    # value equal to zero for rest periods only and not for all I = 0
    for i in range(1, np.size(t, 1)):
        if I[0, i] == 0:
            step_cap_diff[i] = 0
            if step_index[i] != step_index[i - 1]:
                cap_step_temp[i:] = 0  # temporary variable for step capacity
        else:
            cap_step_temp[i:] = cap_step[i:]
            step_cap_diff[i] = np.sign(I[0, i]) * (
                    cap_step_temp[i] - cap_step_temp[i - 1]
            )
    tot_thr = np.cumsum(np.abs(step_cap_diff))
    pos_steps = np.zeros_like(step_cap_diff)
    pos_steps[step_cap_diff > 0.0] = step_cap_diff[step_cap_diff > 0.0]
    charge_thr = np.cumsum(pos_steps)

    # %% Experimental capacity loss
    max_dis_capacity = dis_cap_exp.max()
    cap_loss_exp = (max_dis_capacity - dis_cap_exp) / max_dis_capacity * 100
    cap_loss_exp = np.insert(cap_loss_exp, 0, 0)
    csv_caploss_path = csv_dirc + file_name + "_caploss.csv"
    csvtool.dicttocsv(["Cap Loss"], {"Cap Loss": cap_loss_exp}, csv_caploss_path)

    # %% Capacity loss calculation
    j = 1
    for i in range(0, np.size(temp, 0)):
        while j < np.size(t, 1):

            # New capacity
            #        cap[j] = cap[0] - Q_tot[i,j-1]*cap[0]

            # SOC_step calculation using step capacity with correction
            if step_index[j] != step_index[j - 1]:
                SOC_step[j] = SOC[j - 1]
                if SOC_step[j] > 1:
                    SOC_diff = SOC_step[j] - 1
                    j = index
                    SOC_step[j] = SOC_step[j] - SOC_diff
                    SOC_corr_count += 1
                    corr_inds.append(j)
                elif SOC_step[j] < 0:
                    j = index
                    SOC_step[j] = SOC_step[j] + np.abs(SOC_step[j])
                    SOC_corr_count += 1
                    corr_inds.append(j)
                else:
                    index = j
            else:
                SOC_step[j] = SOC_step[j - 1]

            #        # SOC_step calculation using step capacity without correction
            #        if step_index[j]!= step_index[j-1]:
            #            SOC_step[j] = SOC[j-1]
            #        else:
            #            SOC_step[j] = SOC_step[j-1]

            # SOC calculation
            if I[0, j] == 0:
                SOC[j] = SOC[j - 1]
            else:
                SOC[j] = SOC_step[j] + np.sign(I[0, j]) * cap_step[j] / cap[j]

            j += 1

    print(
        "Total number of SOC corrections are ",
        SOC_corr_count,
        # " at indices", corr_inds
    )

    # %% Writing timeseries_data to csv
    csv_expdata_path = csv_dirc + file_name + "_expdata.csv"
    I = np.ravel(I)
    temp = np.ravel(temp)
    t = np.ravel(t)
    csvtool.dicttocsv(
        ["I", "SOC", "Charge_thr", "Tot_thr", "Temp"],
        {
            "I": I,
            "SOC": SOC,
            "Charge_thr": charge_thr,
            "Tot_thr": tot_thr,
            "Temp": temp,
            "Time [Hr]": t,
            "cycle_index": cycle_index,
        },
        csv_expdata_path,
    )


def run_process(f):
    csv_dirc = (
            r"C:\Users\ssamdani\OneDrive - Farasis Energy Inc\Documents\repos\parameter_optimization\data\b0_sample\processed"
            + "/"
    )
    dirc = r"C:\Users\ssamdani\OneDrive - Farasis Energy Inc\Documents\repos\parameter_optimization\notebooks\data_to_process"
    ts_data = pd.read_csv(f"{dirc}/{f}.csv")
    summary = pd.read_csv(f"{dirc}/{f}__extradata.csv")
    print(f"working on file:{f}")
    create_csv_files(f, ts_data, summary, csv_dirc)


if __name__ == "__main__":
    fnames = [
        # # "432 0.33C 25deg",
        # # "434 1c 25deg",
        # # "310 0.33c 25deg",
        # # "383 1c 25deg",
        # "172",
        # "412",
        # "500",
        "507",
    ]
    pool = Pool(processes=len(fnames))
    pool.map(run_process, fnames)
