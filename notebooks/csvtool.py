"""
This is a general package for doing quick simple things to csv files.
"""
import numpy
import csv

def csvtodict(fname):
    """
    Load the csv file contents from fname and put each column into a list, which
    will be contained in a dict of which the dict keys are defined by the column
    headers.

    For example,
    data_dict[col_0_name] = [ r0_c0, r1_c0, ... rN_c0 ]
    data_dict[col_1_name] = [ r0_c1, r1_c1, ... rN_c1 ]
    ...

    """

    header = ''
    with open(fname, 'r', newline='') as csvfile:
        # Read in each row of the file
        for irow, row in enumerate(csvfile):

            # Set the header as a list with each column name
            if irow == 0 :
                hd = row.split(',')

                header = [hdr.strip('\n').strip('\r') for hdr in hd]

                # Initialize the timeseries_data dict with keys for each column
                data_dict = dict( [ (hdr, []) for hdr in header ] )

            # Fill in the timeseries_data dict with the file contents
            else :
                spl_row = row.split(',')
#                print('spl_row', spl_row)
#                import matplotlib.pyplot as plt
#                plt.figure()
#                plt.show()
                for hdr, val in zip( header, spl_row ) :
                    data_dict[hdr].append( val.strip('\r').strip('\n') )

#    print('header:', header)

    for hdr in header :
        try :
            if 'Index' in hdr or 'index' in hdr:
                dat = numpy.array(data_dict[hdr], dtype='d')
                data_dict[hdr] = numpy.array(dat, dtype='i')  # try to convert the list to a numpy array
            else:
                data_dict[hdr] = numpy.array( data_dict[hdr], dtype='d' )  # try to convert the list to a numpy array
        except :
            print('Was unable to perform the list to numpy array conversion for header:', hdr)

    return data_dict, header

def dicttocsv(old_header, dat_dict, fname):
    """
    write out the timeseries_data dict back to a csv file
    """
    new_keys = list(dat_dict.keys())

    new_header = old_header
    for k in new_keys:
        if k not in old_header:
            new_header.append(k)

    num_rows = len(dat_dict[new_header[0]]) - 1
    with open(fname, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile, delimiter=',')
        csvwriter.writerow(new_header)
        for irow in range(num_rows):
            row = [dat_dict[k][irow] for k in new_header]
            csvwriter.writerow(row)
