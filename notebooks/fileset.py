
from os import listdir
from os.path import isfile, join

def get_files_suffLim( data_path, suffix ) :

    file_set = [ f for f in listdir(data_path) if isfile( join(data_path, f) ) and suffix in f ]

    return file_set


def get_files_suffLim2( data_path, lim0, lim1 ) :

    file_set = [ f for f in listdir(data_path) if isfile( join(data_path, f) ) and lim0 in f and lim1 in f ]

    return file_set

def get_files_suffLim3( data_path, lim0, lim1, lim2 ) :

    file_set = [ f for f in listdir(data_path) if isfile( join(data_path, f) ) and lim0 in f and lim1 in f and lim2 in f ]

    return file_set

def get_files_suffLim4( data_path, lim0, lim1, lim2, lim3 ) :

    file_set = [ f for f in listdir(data_path) if isfile( join(data_path, f) ) and lim0 in f and lim1 in f and lim2 in f and lim3 in f ]

    return file_set
