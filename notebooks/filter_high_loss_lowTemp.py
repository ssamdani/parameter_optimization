"""
this code will be used to filter out high loss values from 0 deg files
it is not tested adn it has been taken from this file:
http://localhost:8888/notebooks/expdata%20file%20verification.ipynb
"""
import pandas as pd
import numpy as np

if __name__ == "__main__":
    # fnames = ["412", "507"]
    # delete_from_cycles = [147,148]
    fnames = [ "507"]
    delete_from_cycles = [148]
    csv_dirc = ( r"..\data\b0_sample\processed"  )
    dirc = r"..\data\b0_sample\processed\temp"
    for i,f in enumerate(fnames):
        summary = pd.read_csv(f"{dirc}/{f}_caploss.csv")
        tsdata = pd.read_csv(f"{dirc}/{f}_expdata.csv")
        filter_loc = delete_from_cycles[i]
        change_cycle = np.ediff1d(tsdata.cycle_index.values,to_begin=1) > 0.99
        cumcycles = np.cumsum(change_cycle)
        location = np.where(cumcycles == filter_loc+1)[0][0]
        print(f'{len(tsdata)}, localtion:{location}')
        tsdata_filtered = tsdata.iloc[:location]
        summary_filtered = summary.iloc[:filter_loc-1]
        tsdata_filtered.to_csv(f'{csv_dirc}/{f}_expdata.csv', index=False)
        summary_filtered.to_csv(f'{csv_dirc}/{f}_caploss.csv', index=False)
