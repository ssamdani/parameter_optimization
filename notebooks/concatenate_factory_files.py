from multiprocessing import Pool
import pandas as pd
import glob


def run_process(cell):
    dirc = r"C:\Users\ssamdani\Documents\data\temp\low_temp_renamed"
    dflist = []
    summary_list = []  # sumfile list
    file_count = len(glob.glob(dirc + f"/*{cell}.xls"))
    print(f"{cell} has {file_count} files")

    for i in range(1, file_count + 1):
        xl = pd.ExcelFile(dirc + f"/{i}_{cell}.xls")
        data_sheet = xl.sheet_names[-1]
        df = pd.read_excel(xl, sheet_name=data_sheet)
        dflist.append(df)
        dfsummary = pd.read_excel(xl, sheet_name="通道循环数据")
        # don't append last line as it contains incorrect discharge capacity values
        summary_list.append(dfsummary[:-1])

    # append last line of the last file
    summary_list.append(dfsummary.iloc[[-1]])

    bigframe = pd.concat(
        dflist,
        # ignore_index=True,
        sort=True,
    )
    summary_frame = pd.concat(
        summary_list,
        # ignore_index=True,
        sort=True,
    )
    writer = pd.ExcelWriter(f"data_to_process/{cell}.xlsx")
    bigframe.to_excel(writer, sheet_name="_1")
    summary_frame.to_excel(writer, sheet_name="summary")
    writer.save()


if __name__ == "__main__":
    cells = ["172", "500", "507", "412"]
    pool = Pool(processes=len(cells))
    pool.map(run_process, cells)
